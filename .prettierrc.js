// https://prettier.io/docs/en/editors.html#visual-studio-code
// https://prettier.io/docs/en/options.html

module.exports = {
  tabWidth: 2,
  useTabs: false,
  singleQuote: true,
  printWidth: 100,
  semi: false,
  trailingComma: 'es5',
}
