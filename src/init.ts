import { Logger } from './util/logger'

let _logger: Logger | null = null
export function getLogger() {
  if (!_logger) _logger = new Logger()
  return _logger!
}
