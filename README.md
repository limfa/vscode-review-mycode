# Review Mycode extension for Visual Studio Code

可指定作者 查看 2 个分支间提交过的文件

比较适合很多开发者同时在一个仓库中开发，在很多提交中 review 自己提交的代码

## Usage

![](https://gitee.com/limfa/vscode-review-mycode/raw/master/doc/usage.gif)

选择 2 个分支（有顺序要求），输入作者，查询到 2 分支间当前作者改动过的文件

![](https://gitee.com/limfa/vscode-review-mycode/raw/master/doc/orange.png)

橙色为当前作者改动过的记录（如果和之前没有变化，可视为回滚操作）

![](https://gitee.com/limfa/vscode-review-mycode/raw/master/doc/blue.png)

蓝色为非当前作者改动的记录
